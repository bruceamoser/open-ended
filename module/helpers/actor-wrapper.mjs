export class ActorWrapper {
  constructor(actor) {
    this.actorData = actor.data;
    this.data = this.actorData.data;

    this.data.kin = this.actorData.items.find((o) => o.type == "Kin");
    this.data.vocation = this.actorData.items.find((o) => o.type == "Vocation");
    this.data.culture = this.actorData.items.find((o) => o.type == "Culture");
    this.data.background_options = this.actorData.items.filter((o) => o.type == "Background-Option");
    this.data.special_abilities = this.actorData.items.filter((o) => o.type == "Special-Ability");
    this.data.languages = this.actorData.items.filter((o) => o.type == "Language");
    this.data.weapons = this.actorData.items.filter((o) => o.type == "Weapon");
    this.data.armor = this.actorData.items.filter((o) => o.type == "Armor");
    this.data.equipment = this.actorData.items.filter((o) => o.type == "Equipment");
  }

  
  _calculateResistances() {
    const data = this.data;
    for (let key in data.resistances) {
      const r = data.resistances[key];
      if (r.type == "Resistance") {
        const resistance = r.data.data;
        const statKey = resistance.stat;
        const ability = this.getStatByKey(statKey);
        resistance.stat_bonus = ability.data.data.total_bonus;
        resistance.level_bonus = data.level.value * 5;
        resistance.total_bonus =
          resistance.stat_bonus +
          resistance.kin_bonus +
          resistance.vocation_bonus +
          resistance.culture_bonus +
          resistance.background_bonus +
          resistance.item_bonus +
          resistance.special_bonus +
          resistance.level_bonus;
      }
    }
  }

  _calculateStats() {
    const data = this.data;
    for (let key in data.stats) {
      const s = data.stats[key];
      if (s.type == "Stat") {
        const stat = s.data.data;
        stat.total_bonus =
          stat.base_bonus +
          stat.kin_bonus +
          stat.vocation_bonus +
          stat.culture_bonus +
          stat.background_bonus +
          stat.item_bonus +
          stat.special_bonus;
      }
    }
  }

  _calculateSkills() {
    const data = this.data;
    for (let key in data.skills) {
      const skill = data.skills[key];
      if (skill.data.data.total_bonus != undefined) {
        const skillData = skill.data;
        const statKey = skillData.data.stat;
        const ability = this.getStatByKey(statKey);
        skillData.data.stat_bonus = ability.data.data.total_bonus;
        skillData.data.rank_bonus =
          (skillData.data.earned_ranks + skillData.data.number_ranks) * 5;
        skillData.data.total_bonus =
          skillData.data.rank_bonus +
          skillData.data.kin_bonus +
          skillData.data.stat_bonus +
          skillData.data.vocation_bonus +
          skillData.data.item_bonus +
          skillData.data.culture_bonus +
          skillData.data.background_bonus +
          skillData.data.special_bonus;
      }
    }
  }
  _populateBaseStats() {
    const length = Object.values(this.data.stats).length;
    if (length < 1 ) {
      const statsArray = game.items.filter(f => f.type == 'Stat');
      for (let stat of statsArray) {
        const item = this.actor.createEmbeddedDocuments("Item", [stat.data]);
      }
    } 
  }

  _populateBaseSkills() {
    const skillLength = Object.values(this.data.skills).length;
    if (skillLength < 1 ) {
      const skillsArray = game.items.filter(f => f.type == 'Skill');
      for (let skill of skillsArray) {
        const item = this.actor.createEmbeddedDocuments("Item", [skill.data]);
      }
    }

  }

  _populateBaseResistances() {
    const resistanceLength = Object.values(this.data.resistances).length;
    if (resistanceLength < 1 ) {
      const resistancesArray = game.items.filter(f => f.type == 'Resistance');
      for (let resistance of resistancesArray) {
        const item = this.actor.createEmbeddedDocuments("Item", [resistance.data]);
      }
    }
  }

  populateBaseData() {
    this._populateBaseStats();
    this._populateBaseSkills();
    this._populateBaseResistances();
  }

  prepareData() {
    this._calculateStats();
    this._calculateResistances();
    this._calculateSkills();
  }

  getStatByKey(aKey) {
    for (let key in this.data.stats) {
      const stat = this.data.stats[key];
      if (stat.type == "Stat") {
        if (stat.data.data.key == aKey) {
          return stat;
        }
      }
    }
    return null;
  }
}
