/**
 * Define a set of template paths to pre-load
 * Pre-loaded templates are compiled and cached for fast access when rendering
 * @return {Promise}
 */
 export const preloadHandlebarsTemplates = async function() {
  return loadTemplates([

    // Actor partials.
    "systems/openended/templates/actor/parts/actor-equipment.html",
    "systems/openended/templates/actor/parts/actor-weapons.html",
    "systems/openended/templates/actor/parts/actor-armor.html",
    "systems/openended/templates/actor/parts/actor-languages.html",
    "systems/openended/templates/actor/parts/actor-skills.html",
    "systems/openended/templates/actor/parts/actor-stats.html",
    "systems/openended/templates/actor/parts/actor-resistance.html",
    "systems/openended/templates/actor/parts/actor-effects.html",
    "systems/openended/templates/actor/parts/actor-background-options.html",
    "systems/openended/templates/actor/parts/actor-special-abilities.html",

    // Item partials.
    "systems/openended/templates/item/parts/item-effects.html",

  ]);
};
