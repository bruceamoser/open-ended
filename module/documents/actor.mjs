
/**
 * Extend the base Actor document by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class OpenEndedSystemActor extends Actor {

  /** @override */
  prepareData() {
    // Prepare data for the actor. Calling the super version of this executes
    // the following, in order: data reset (to clear active effects),
    // prepareBaseData(), prepareEmbeddedDocuments() (including active effects),
    // prepareDerivedData().
    super.prepareData();
  }

  /** @override */
  prepareBaseData() {
    // Data modifications in this step occur before processing embedded
    // documents or derived data.
    // **** add data structure here ****
    
    const actorData = this.data;
    const data = actorData.data;

    data.skills = {};
    data.stats = {};
    data.skillGroups = {};
    data.resistances = {};
    data.kin = {};
    data.vocation = {};
    data.culture = {};
    data.background_options = [];
    data.special_abilities = [];
    data.languages = [];
    data.weapons = [];
    data.armor = [];
    data.equipment = [];

    this._copyStatsFromItems(actorData);
    this._copyResistancesFromItems(actorData);
    this._copySkillGroupsSkillsFromItems(actorData);

  }

  /**
   * @override
   * Augment the basic actor data with additional dynamic data. Typically,
   * you'll want to handle most of your calculated/derived data in this step.
   * Data calculated in this step should generally not exist in template.json
   * (such as ability modifiers rather than ability scores) and should be
   * available both inside and outside of character sheets (such as if an actor
   * is queried and has a roll executed directly from it).
   * ******* add data calculations to structure ******
   */
  prepareDerivedData() {
    const actorData = this.data;
    const data = actorData.data;
    const flags = actorData.flags.openended || {};

    // Make separate methods for each Actor type (character, npc, etc.) to keep
    // things organized.
    this._preparePCData(actorData);
    this._prepareNpcData(actorData);
  }

  /**
   * Prepare Character type specific data
   */
  _preparePCData(actorData) {
    if (actorData.type !== 'PC') return;

    // Make modifications to data here. For example:
    const data = actorData.data;
    //this.actorWrapper.prepareData();
  }

  /**
   * Prepare NPC type specific data.
   */
  _prepareNpcData(actorData) {
    if (actorData.type !== 'NPC') return;

    // Make modifications to data here. For example:
    const data = actorData.data;
  }

  /**
   * Override getRollData() that's supplied to rolls.
   */
  getRollData() {
    const data = super.getRollData();

    // Prepare character roll data.
    this._getCharacterRollData(data);
    this._getNpcRollData(data);

    return data;
  }

  /**
   * Prepare character roll data.
   */
  _getCharacterRollData(data) {
    if (this.data.type !== 'PC') return;

    // Copy the ability scores to the top level, so that rolls can use
    // formulas like `@str.mod + 4`.

  }

  /**
   * Prepare NPC roll data.
   */
  _getNpcRollData(data) {
    if (this.data.type !== 'NPC') return;

    // Process additional NPC data here.
  }

  _copyStatsFromItems(actorData) {
    const data = actorData.data;
    const statsArray = actorData.items
      .filter((o) => o.type == "Stat")
      .sort((a, b) => (a.data.name > b.data.name ? 1 : -1));
    for (let stat of statsArray) {
      data.stats[stat.data.data.key] = stat;
    }
  }

  _copyResistancesFromItems(actorData) {
    const data = actorData.data;
    const resistanceArray = actorData.items
      .filter((o) => o.type == "Resistance")
      .sort((a, b) => (a.data.name > b.data.name ? 1 : -1));
    for (let resistance of resistanceArray) {
      data.resistances[resistance.data.name] = resistance;
    }
  }

  _copySkillGroupsSkillsFromItems(actorData) {
    const data = actorData.data;
    const skillArray = actorData.items
      .filter((o) => o.type == "Skill")
      .sort((a, b) => (a.data.name > b.data.name ? 1 : -1));
    for (let skill of skillArray) {
      data.skills[skill.name] = skill;
      if (!data.skillGroups[skill.data.data.skill_group]) {
        data.skillGroups[skill.data.data.skill_group] = {};
        data.skillGroups[skill.data.data.skill_group].development_ponts = 0;
        data.skillGroups[skill.data.data.skill_group].skills = {};
        data.skillGroups[skill.data.data.skill_group].name = skill.data.data.skill_group;
      }
      data.skillGroups[skill.data.data.skill_group].skills[skill.name] = skill;
    }
  }

}