import {onManageActiveEffect, prepareActiveEffectCategories} from "../helpers/effects.mjs";
/**
 * Extend the basic ItemSheet with some very simple modifications
 * @extends {ItemSheet}
 */
export class OpenEndedSystemItemSheet extends ItemSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["openended", "sheet", "item"],
      width: 520,
      height: 480,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description" }]
    });
  }

  /** @override */
  get template() {
    const path = "systems/openended/templates/item";
    // Return a single sheet for all item types.
    // return `${path}/item-sheet.html`;

    // Alternatively, you could use the following return statement to do a
    // unique item sheet by type, like `weapon-sheet.html`.
    return `${path}/item-${this.item.data.type}-sheet.html`;
  }

  /* -------------------------------------------- */

  /** @override */
  getData() {
    // Retrieve base data structure.
    const context = super.getData();

    // Use a safe clone of the item data for further operations.
    //const itemData = context.item.data;
    const itemData = this.item.data.toObject(false);
    const data = itemData.data;
    // Retrieve the roll data for TinyMCE editors.
    context.rollData = {};
    let actor = this.object?.parent ?? null;
    if (actor) {
      context.rollData = actor.getRollData();
    }

    if (itemData.type == "Skill" || itemData.type == "Resistance") {
      const tempStats = {};
      const stats = game.items.filter(f => f.type == 'Stat');
      for (let key in stats) {
        const stat = stats[key];
        if (stat.type == 'Stat') {
          tempStats[stat.data.name] = stat.data.data.key;
        }
      }
      data.tempStats = tempStats;
    }

    if (itemData.type == "Skill") {
      const tempSkillGroups = {};
      const skillGroups = game.items.filter(f => f.type == "Skill-Group");
      for (let key in skillGroups) {
        if (!isNaN(key)) {
          const skillGroup = skillGroups[key];
          tempSkillGroups[skillGroup.name] = skillGroup.data.data.key;
        }
      }
      data.tempSkillGroups = tempSkillGroups;
    }

    // Add the actor's data to context.data for easier access, as well as flags.
    context.data = itemData.data;
    context.flags = itemData.flags;
    context.stat = data.stat;

    // Prepare active effects
    context.effects = prepareActiveEffectCategories(this.item.effects);
    return context;
  }


  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Render the item sheet for viewing/editing prior to the editable check.
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.item.data.data.modifiers.get(li.data("itemId"));
      item.sheet.render(true);
    });

    html.find('.ability-selector').change(ev => {
      let i = game.items.get(ev.currentTarget.id);
      i.data.data.stat = ev.currentTarget.value;
      i.update({"data.stat": ev.currentTarget.value});
    })
    html.find('.skill-group-selector').change(ev => {
      let i = game.items.get(ev.currentTarget.id);
      i.data.data.skill_group = ev.currentTarget.value;
      i.update({"data.skill_group": ev.currentTarget.value});
    })
    // Everything below here is only needed if the sheet is editable
    if (!this.isEditable) return;

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.item.items.get(li.data("itemId"));
      item.delete();
      li.slideUp(200, () => this.render(false));
    });

    // Active Effect management
    html.find(".effect-control").click(ev => onManageActiveEffect(ev, this.item));

    if (this.item.isOwner) {
      let handler = ev => this._onDragStart(ev);
      html.find('li.item').each((i, li) => {
        if (li.classList.contains("inventory-header")) return;
        li.setAttribute("draggable", true);
        li.addEventListener("dragstart", handler, false);
      });
    }    
  }

}
